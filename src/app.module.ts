import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Menu } from './menus/entities/menu.entity';
import { MenusModule } from './menus/menus.module';
import { Serve } from './serve/entities/serve.entity';
import { ServeModule } from './serve/serve.module';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { BasketsModule } from './baskets/baskets.module';
import { PaymentModule } from './payment/payment.module';
import { Payment } from './payment/entities/payment.entity';
import { Basket } from './baskets/entities/basket.entity';
import { OrderitemsModule } from './orderitems/orderitems.module';
import { Orderitem } from './orderitems/entities/orderitem.entity';
import { SalaryModule } from './salary/salary.module';
import { Salary } from './salary/entities/salary.entity';
import { StockModule } from './stock/stock.module';
import { Stock } from './stock/entities/stock.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'topkung493.ddns.net',
      port: 3306,
      username: 'LazyBunnies',
      password: 'Lazy@212317',
      database: 'project_lazy',
      entities: [
        Menu,
        Table,
        Serve,
        Order,
        Payment,
        Basket,
        Orderitem,
        Salary,
        Stock,
      ],
      synchronize: true,
    }),
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   logging: false,
    //   entities: [Menu],
    //   migrations: [],
    //   subscribers: [],
    // }),
    MenusModule,
    TablesModule,
    AuthModule,
    ServeModule,
    OrdersModule,
    BasketsModule,
    PaymentModule,
    OrderitemsModule,
    SalaryModule,
    StockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
